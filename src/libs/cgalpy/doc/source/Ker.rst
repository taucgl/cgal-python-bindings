Bindings for CGAL 2d/3D Kernel Module
=====================================

.. automodule:: @SPHINX_TARGET_NAME@.Ker
    :members:
    :undoc-members:
