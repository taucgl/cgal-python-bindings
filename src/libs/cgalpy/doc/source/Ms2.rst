Bindings for CGAL 2D Minkowski Sum Module
==========================================

.. automodule:: @SPHINX_TARGET_NAME@.Ms2
    :members:
    :undoc-members:
