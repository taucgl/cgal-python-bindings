Bindings for CGAL 2D Polygon Partitioning Module
=================================================

.. automodule:: @SPHINX_TARGET_NAME@.Pp2
    :members:
    :undoc-members:
