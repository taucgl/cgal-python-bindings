Bindings for CGAL 3D Polyhedron Module
======================================

.. automodule:: @SPHINX_TARGET_NAME@.Pol3
    :members:
    :undoc-members:
