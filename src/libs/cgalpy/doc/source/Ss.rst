Bindings for CGAL Spatial Search Module
========================================

.. automodule:: @SPHINX_TARGET_NAME@.Ss
    :members:
    :undoc-members:
