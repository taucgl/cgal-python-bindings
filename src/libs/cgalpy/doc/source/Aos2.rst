Bindings for CGAL 2D Arrangement On Surface Module
==================================================

.. automodule:: @SPHINX_TARGET_NAME@.Aos2
    :members:
    :undoc-members:
