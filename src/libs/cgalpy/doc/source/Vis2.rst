Bindings for CGAL 2D Visibility Module
======================================

.. automodule:: @SPHINX_TARGET_NAME@.Vis2
    :members:
    :undoc-members:
