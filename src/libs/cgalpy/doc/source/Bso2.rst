Bindings for CGAL 2D Boolean Set Operations Module
===================================================

.. automodule:: @SPHINX_TARGET_NAME@.Bso2
    :members:
    :undoc-members:
