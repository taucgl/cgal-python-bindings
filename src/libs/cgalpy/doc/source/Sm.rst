Bindings for CGAL Surface Mesh Module
=====================================

.. automodule:: @SPHINX_TARGET_NAME@.Sm
    :members:
    :undoc-members:
