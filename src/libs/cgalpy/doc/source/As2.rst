Bindings for CGAL 2d Alpha Shape
=====================================

.. automodule:: @SPHINX_TARGET_NAME@.As2
    :members:
    :undoc-members:
