Bindings for CGAL 2D Convex Hull Module
========================================

.. automodule:: @SPHINX_TARGET_NAME@.Ch2
    :members:
    :undoc-members:
