Bindings for CGAL 2D Polygons Module
=====================================

.. automodule:: @SPHINX_TARGET_NAME@.Pol2
    :members:
    :undoc-members:
