Bindings for CGAL d-Kernel Module
===================================

.. automodule:: @SPHINX_TARGET_NAME@.Kerd
    :members:
    :undoc-members:
