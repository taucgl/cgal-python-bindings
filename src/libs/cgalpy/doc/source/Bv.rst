Bindings for CGAL Bounding Volume Module
=========================================

.. automodule:: @SPHINX_TARGET_NAME@.Bv
    :members:
    :undoc-members:
