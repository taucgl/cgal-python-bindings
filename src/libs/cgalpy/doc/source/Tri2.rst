Bindings for CGAL 2D Triangulation Module
==========================================

.. automodule:: @SPHINX_TARGET_NAME@.Tri2
    :members:
    :undoc-members:
