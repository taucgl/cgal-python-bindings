Bindings for CGAL Polygon mesh processing Module
================================================

.. automodule:: @SPHINX_TARGET_NAME@.Pmp
    :members:
    :undoc-members:
