// Copyright (c) 2023 Israel.
// All rights reserved to Tel Aviv University.
//
// SPDX-License-Identifier: GPL-3.0-or-later.
// Commercial use is authorized only through a concession contract to purchase a commercial license for CGAL.
//
// Author(s): Efi Fogel         <efifogel@gmail.com>

#ifndef CGALPY_CONVEX_HULL_3_TYPES_HPP
#define CGALPY_CONVEX_HULL_2_TYPES_HPP

#include "CGALPY/convex_hull_3_config.hpp"

namespace ch3 {

using Polygonal_mesh = Poly_mesh<CGALPY_CH3_POLYGONAL_MESH>::type;

}

#endif
