// Copyright (c) 2019 Israel.
// All rights reserved to Tel Aviv University.
//
// SPDX-License-Identifier: GPL-3.0-or-later.
// Commercial use is authorized only through a concession contract to purchase a commercial license for CGAL.
//
// Author(s): Nir Goren         <nirgoren@mail.tau.ac.il>
//            Efi Fogel         <efifogel@gmail.com>

#ifndef CGALPY_GENERAL_DISTANCE_PYTHON_HPP
#define CGALPY_GENERAL_DISTANCE_PYTHON_HPP

#include <nanobind/nanobind.h>

#include "CGALPY/Python_functor.hpp"

#include <CGAL/Kd_tree_rectangle.h>

namespace py = nanobind;

template <typename D_, typename FT_, typename Point_d_, typename Query_item_>
class General_distance_python {
public:
  using D = D_;
  using FT = FT_;
  using Point_d = Point_d_;
  using Query_item = Query_item_;
  using Kd_tree_rectangle = CGAL::Kd_tree_rectangle<FT, D>;

private:
  Python_functor_2_ref<Query_item, Point_d, FT> f0;
  Python_functor_2_ref<Query_item, Kd_tree_rectangle, FT> f1;
  Python_functor_2_ref<Query_item, Kd_tree_rectangle, FT> f2;
  Python_functor_1<FT, FT> f3;
  Python_functor_1<FT, FT> f4;

public:
  General_distance_python() {}

  General_distance_python(py::object py_functor0, py::object py_functor1,
    py::object py_functor2, py::object py_functor3, py::object py_functor4)
  {
    f0 = Python_functor_2_ref<Query_item, Point_d, FT>(py_functor0);
    f1 = Python_functor_2_ref<Query_item, Kd_tree_rectangle, FT>(py_functor1);
    f2 = Python_functor_2_ref<Query_item, Kd_tree_rectangle, FT>(py_functor2);
    f3 = Python_functor_1<FT, FT>(py_functor3);
    f4 = Python_functor_1<FT, FT>(py_functor4);
  }

  FT transformed_distance(const Query_item& q, const Point_d& r) const
  {
    return f0(q, r);
  }

  FT min_distance_to_rectangle(const Query_item& q, const Kd_tree_rectangle& r) const
  {
    return f1(q, r);
  }

  FT max_distance_to_rectangle(const Query_item& q, const Kd_tree_rectangle& r) const
  {
    return f2(q, r);
  }

  FT transformed_distance(const FT& d) const
  {
    return f3(d);
  }

  FT inverse_of_transformed_distance(const FT& d) const
  {
    return f4(d);
  }
};

#endif // CGALPY_GENERAL_DISTANCE_PYTHON_HPP
