// Copyright (c) 2023 Israel.
// All rights reserved to Tel Aviv University.
//
// SPDX-License-Identifier: GPL-3.0-or-later.
// Commercial use is authorized only through a concession contract to purchase a commercial license for CGAL.
//
// Author(s): Efi Fogel         <efifogel@gmail.com>

#ifndef CGALPY_SURFACE_SWEEP_2_TYPES_HPP
#define CGALPY_SURFACE_SWEEP_2_TYPES_HPP

#include "CGALPY/kernel_types.hpp"
#include "CGALPY/arrangement_on_surface_2_types.hpp"

namespace ss2 {

using Geometry_traits_2 = aos2::Geometry_traits_2;
using Point_2 = aos2::Point_2;
using Curve_2 = aos2::Curve_2;
using X_monotone_curve_2 = aos2::X_monotone_curve_2;

}

#endif
