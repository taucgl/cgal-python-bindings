// Copyright (c) 2022 Israel.
// All rights reserved to Tel Aviv University.
//
// SPDX-License-Identifier: GPL-3.0-or-later.
// Commercial use is authorized only through a concession contract to purchase a commercial license for CGAL.
//
// Author(s): Efi Fogel         <efifogel@gmail.com>

#ifndef CGALPY_POLYGON_MESH_PROCESSING_TYPES_HPP
#define CGALPY_POLYGON_MESH_PROCESSING_TYPES_HPP

#include "CGALPY/polygon_mesh_processing_config.hpp"

namespace pmp {

using Polygonal_mesh = Poly_mesh<CGALPY_PMP_POLYGONAL_MESH>::type;

}

#endif
