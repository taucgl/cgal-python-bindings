// Copyright (c) 2025 Israel.
// All rights reserved to Tel Aviv University.
//
// SPDX-License-Identifier: GPL-3.0-or-later.
// Commercial use is authorized only through a concession contract to purchase a commercial license for CGAL.
//
// Author(s): Efi Fogel         <efifogel@gmail.com>

#include <nanobind/nanobind.h>

#include "CGALPY/add_attr.hpp"
#include "CGALPY/kernel_types.hpp"
#include "CGALPY/polyhedron_3_types.hpp"

namespace py = nanobind;

namespace pol3 {

const Halfedge& opposite(const Halfedge& h) { return *(h.opposite()); }
const Halfedge& next(const Halfedge& h) { return *(h.next()); }
const Halfedge& next_on_vertex(const Halfedge& h) { return *(h.next_on_vertex()); }
const Halfedge& prev(const Halfedge& h) { return *(h.prev()); }
const Halfedge& prev_on_vertex(const Halfedge& h) { return *(h.prev_on_vertex()); }
const Face& face(const Halfedge& h) { return *(h.face()); }
const Face& facet(const Halfedge& h) { return *(h.facet()); }
const Vertex& vertex(const Halfedge& h) { return *(h.vertex()); }

}

// Export Polyhedron Halfedge
void export_polyhedron_halfedge(py::class_<pol3::Polyhedron_3>& prn_c) {
  using Prn = pol3::Polyhedron_3;
  using Halfedge = Prn::Halfedge;
  constexpr auto ri(py::rv_policy::reference_internal);

  if (add_attr<Halfedge>(prn_c, "Halfedge")) return;

  py::class_<Halfedge> halfedge_c(prn_c, "Halfedge");
  halfedge_c.def(py::init<>())
    .def("facet", &pol3::facet, ri)
    .def("face", &pol3::face, ri)
    .def("facet_degree", [](const Halfedge& h) { return h.facet_degree(); })
    .def("is_border", [](const Halfedge& h) { return h.is_border(); })
    .def("is_border_edge", [](const Halfedge& h) { return h.is_border_edge(); })
    .def("is_quad", [](const Halfedge& h) { return h.is_quad(); })
    .def("is_bivalent", [](const Halfedge& h) { return h.is_bivalent(); })
    .def("is_triangle", [](const Halfedge& h) { return h.is_triangle(); })
    .def("is_trivalent", [](const Halfedge& h){  return h.is_trivalent(); })
    .def("next", &pol3::next, ri)
    .def("next_on_vertex", &pol3::next_on_vertex, ri)
    .def("prev", &pol3::prev, ri)
    .def("prev_on_vertex", &pol3::prev_on_vertex, ri)
    .def("opposite", &pol3::opposite, ri)
    .def("vertex", &pol3::vertex, ri)
    .def("vertex_degree", [](const Halfedge& h) { return h.vertex_degree(); })
#ifdef CGALPY_POL3_HALFEDGE_EXTENDED
    // The member functions set_data() and data() are defined in a base class of
    // Halfedge. Therefore, we cannot directly refere to any of them, e.g.,
    // `Halfedge::set_data`. Instead, we introduce lambda functions that call
    // the appropriate member functions.
    .def("set_data", [](Halfedge& h, py::object obj) { h.set_data(obj); })
    .def("data", [](const Halfedge& h)->py::object { return h.data(); })
#endif
    ;

}
