// Copyright (c) 2019 Israel.
// All rights reserved to Tel Aviv University.
//
// SPDX-License-Identifier: GPL-3.0-or-later.
// Commercial use is authorized only through a concession contract to purchase a commercial license for CGAL.
//
// Author(s): Nir Goren         <nirgoren@mail.tau.ac.il>
//            Efi Fogel         <efifogel@gmail.com>

#include <nanobind/nanobind.h>
#include <nanobind/stl/unique_ptr.h>

#include <CGAL/Envelope_3/Envelope_base.h>

#include "CGALPY/arrangement_on_surface_2_types.hpp"
#include "CGALPY/make_iterator.hpp"
#include "CGALPY/make_circulator.hpp"
#include "CGALPY/add_attr.hpp"

namespace py = nanobind;

namespace aos2 {

//!
py::object incident_halfedges_circulator(const Vertex& v)
{ return make_circulator(v.incident_halfedges()); }

//
py::object incident_halfedges_iterator(const Vertex& v)
{ return make_iterator(v.incident_halfedges(), v.incident_halfedges()); }

//
#ifdef CGALPY_ENVELOPE_3_BINDINGS
py::object surfaces(const Vertex& v)
{ return make_iterator(v.surfaces_begin(), v.surfaces_end()); }
#endif

}

//
void export_vertex(py::class_<aos2::Arrangement_on_surface_2>& c) {
  using Aos = aos2::Arrangement_on_surface_2;
  using Vertex = Aos::Vertex;
  using Face = Aos::Face;
  using Point = Aos::Point_2;
  constexpr auto ri(py::rv_policy::reference_internal);

#ifdef CGALPY_ENVELOPE_3_BINDINGS
  using Env_data = Vertex::Vertex_data;
  using Dd = CGAL::Dac_decision;
#endif

  if (add_attr<Vertex>(c, "Vertex")) return;
  py::class_<Vertex> vertex_c(c, "Vertex");
  vertex_c.def(py::init<>())

    // As a convention, add the suffix `_mutable` to the mutable version.
    // Wrap the mutable method with the `reference_internal` call policy.
    // An unsafe point that is referenced counted will most likely die when the
    // Aos data structure that holds it dies, as the reference counter will
    // vanish. Not all points are referenced counted (perhaps they should...).
    .def("point_unsafe_mutable", [](Vertex& v)->Point& { return v.point(); }, ri)
    .def("point_unsafe", [](const Vertex& v)->const Point& { return v.point(); }, ri)
    .def("point",
         [](const Vertex& v)->std::unique_ptr<Point>
         { return std::make_unique<Point>(v.point()); }, ri)
    .def("is_isolated", [](const Vertex& v)->bool { return v.is_isolated(); })

    // Immediate members
    .def("is_at_open_boundary", &Vertex::is_at_open_boundary)
    .def("parameter_space_in_x",
         [](const Vertex& v)->CGAL::Arr_parameter_space
         { return v.parameter_space_in_x(); })
    .def("parameter_space_in_y",
         [](const Vertex& v)->CGAL::Arr_parameter_space
         { return v.parameter_space_in_y(); })
    .def("degree", &Vertex::degree)
    .def("face", [](const Vertex& v)->const Face& { return *(v.face()); }, ri)
    .def("incident_halfedges",
         &aos2::incident_halfedges_circulator, py::keep_alive<0, 1>())
    .def("incident_halfedges_range",
         &aos2::incident_halfedges_iterator, py::keep_alive<0, 1>())

#ifdef CGALPY_AOS2_VERTEX_EXTENDED
    // The member functions set_data() and data() are defined in a base class of
    // Face. Therefore, we cannot directly refere to any of them, e.g.,
    // `Face::set_data`. Instead, we introduce lambda functions that call the
    // appropriate member functions.
    .def("set_data", [](Vertex& v, py::object obj) { v.set_data(obj); })
    .def("data", [](const Vertex& v)->py::object { return v.data(); })
#endif

#ifdef CGALPY_ENVELOPE_3_BINDINGS
    .def("number_of_surfaces", [](Vertex& v) { return v.number_of_surfaces(); })
    .def("surfaces", &aos2::surfaces, py::keep_alive<0, 1>())
#endif
    ;

#ifdef CGALPY_ENVELOPE_3_BINDINGS
  using Si = Vertex::Data_const_iterator;
  add_iterator<Si, Si>("Surface_iterator", vertex_c);
#endif

  using Havcc = Aos::Halfedge_around_vertex_const_circulator;
  add_circulator<Havcc>("Halfedge_around_vertex_circulator", vertex_c);
  add_iterator_from_circulator<Havcc>("Halfedge_around_vertex_iterator", vertex_c);
}
