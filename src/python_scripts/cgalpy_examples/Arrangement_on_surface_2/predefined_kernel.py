#!/usr/bin/python

import os
import sys
import importlib
import time
from read_objects import *
from arr_print import *

if len(sys.argv) < 2: lib = 'CGALPY'
else: lib = sys.argv[1]

CGALPY = importlib.import_module(lib)
Ker = CGALPY.Ker
Aos2 = CGALPY.Aos2
Arrangement = Aos2.Arrangement_2
Segment = Arrangement.Curve_2

# Get the name of the input file from the command line, or use the default
# fan_grids.dat file if no command-line parameters are given.
try:
  filename = argv[1]
except:
  filename = 'fan_grids.dat'

# Open the input file.
segments = read_objects(Segment, filename)

# Construct the arrangement by aggregately inserting all segments.
arr = Arrangement()

print('Performing aggregated insertion of {} segments.'.format(len(segments)))
tic = time.perf_counter()
Aos2.insert(arr, segments)
toc = time.perf_counter()
print_arrangement_size(arr)
print(f"Construction took {toc - tic:0.4f} seconds")
