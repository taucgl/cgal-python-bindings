#!/usr/bin/python

import os
import sys
import importlib

lib = 'CGALPY'
i = 1
if len(sys.argv) > 1:
  str = sys.argv[1]
  if str.startswith('CGALPY'):
    lib = str
    i = 2

CGALPY = importlib.import_module(lib)
Sm = CGALPY.Sm
Pmp = CGALPY.Pmp

filename = sys.argv[i] if len(sys.argv) > i else CGALPY.data_file_path("meshes/eight.off")
try: mesh = Sm.read_polygon_mesh(filename)
except: raise ValueError("Invalid input.")

points_per_face = 10 if len(sys.argv) < 2 else int(sys.argv[1])

points = Pmp.sample_triangle_mesh(mesh, {"number_of_points_per_face": points_per_face})

point_set = set(Pmp.sample_triangle_mesh(mesh))

print(len(point_set))
