set(CGALPY_USE_SHARED_LIBS on CACHE BOOL "" FORCE)
set(CGALPY_KERNEL_NAME "epec" CACHE STRING "use EPEC kernel" FORCE)
set(CGALPY_KERNEL_INTERSECTION_BINDINGS on CACHE BOOL "with intersections" FORCE)
set(CGALPY_POLYHEDRON_3_BINDINGS on CACHE BOOL "" FORCE)
set(CGALPY_POL3_GEOMETRY_TRAITS_NAME "withNormals" CACHE STRING "with normals" FORCE)
set(CGALPY_CONVEX_HULL_3_BINDINGS on CACHE BOOL "" FORCE)
set(CGALPY_CH3_POLYGONAL_MESH_NAME "polyhedron" CACHE STRING "" FORCE)
