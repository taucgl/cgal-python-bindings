set(CGALPY_USE_SHARED_LIBS on CACHE BOOL "" FORCE)
set(CGALPY_KERNEL_NAME "epec" CACHE STRING "use EPEC kernel" FORCE)
set(CGALPY_ARRANGEMENT_ON_SURFACE_2_BINDINGS on CACHE BOOL "" FORCE)
set(CGALPY_AOS2_POINT_LOCATION_BINDINGS on CACHE BOOL "" FORCE)
set(CGALPY_VISIBILITY_2_BINDINGS on CACHE BOOL "2D Visibility bindings" FORCE)
set(CGALPY_VIS2_REGULARIZATION_CATEGORY on CACHE BOOL "" FORCE)
