set(CGALPY_USE_SHARED_LIBS on CACHE BOOL "" FORCE)
set(CGALPY_KERNEL_NAME "epic" CACHE STRING "use EPIC kernel" FORCE)
set(CGALPY_POLYHEDRON_3_BINDINGS on CACHE BOOL "" FORCE)
set(CGALPY_PMP_POLYGONAL_MESH_NAME "surfaceMesh" CACHE STRING "" FORCE)
set(CGALPY_TRIANGULATED_SURFACE_MESH_SEGMENTATION_BINDINGS on CACHE BOOL "" FORCE)
