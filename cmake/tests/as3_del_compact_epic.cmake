set(CGALPY_USE_SHARED_LIBS on CACHE BOOL "" FORCE)
set(CGALPY_KERNEL_NAME "epic" CACHE STRING "" FORCE)
set(CGALPY_TRIANGULATION_3_BINDINGS on CACHE BOOL "" FORCE)
set(CGALPY_TRI3_NAME "delaunay" CACHE STRING "" FORCE)
set(CGALPY_TRI3_LOCATION_POLICY_NAME "compact" CACHE STRING "" FORCE)
set(CGALPY_ALPHA_SHAPE_3_BINDINGS on CACHE BOOL "" FORCE)

# set(CAKE_VERBOSE_MAKEFILE on CACHE BOOL "" FORCE)
# set(CMAKE_INSTALL_PREFIX ${SNAPFIXT_RELEASE_ROOT CACHE STRING "" FORCE)
