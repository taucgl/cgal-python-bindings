set(CGALPY_USE_SHARED_LIBS on CACHE BOOL "" FORCE)
set(CGALPY_KERNEL_NAME "epec" CACHE STRING "use EPEC kernel" FORCE)
set(CGALPY_KERNEL_INTERSECTION_BINDINGS on CACHE BOOL "with intersections" FORCE)
set(CGALPY_ARRANGEMENT_ON_SURFACE_2_BINDINGS on CACHE BOOL "" FORCE)
set(CGALPY_AOS2_GEOMETRY_TRAITS_NAME "circleSegment" CACHE STRING "use circular arcs & segments" FORCE)
set(CGALPY_BOOLEAN_SET_OPERATIONS_2_BINDINGS on CACHE BOOL "" FORCE)
