if(NOT TRIANGULATION_3_OPTIONS_FILE_INCLUDED)
set(TRIANGULATION_3_OPTIONS_FILE_INCLUDED)

######## 3D Triangulations types
set(CGALPY_TRI3_PLAIN             0)
set(CGALPY_TRI3_REGULAR           1)
set(CGALPY_TRI3_DELAUNAY          2)
set(CGALPY_TRI3_PERIODIC_PLAIN    3)
set(CGALPY_TRI3_PERIODIC_REGULAR  4)
set(CGALPY_TRI3_PERIODIC_DELAUNAY 5)

set(CGALPY_TRI3_SHORT_NAMES plain reg del pp preg pdel)
set(CGALPY_TRI3_NAMES plain regular delaunay periodicPlain periodicRegular periodicDelaunay)

# Default
SET(CGALPY_TRI3 ${CGALPY_TRI3_PLAIN} CACHE INTERNAL "")
SET(CGALPY_TRI3_NAME "plain" CACHE STRING "The 3D Triangulation")
set_property(CACHE CGALPY_TRI3_NAME PROPERTY STRINGS plain regular delaunay periodicPlain periodicRegular periodicDelaunay)

# 3D Triangulation concurrency
set(CGALPY_TRI3_CONCURRENCY_SEQUENTIAL 0)
set(CGALPY_TRI3_CONCURRENCY_PARALLEL   1)

set(CGALPY_TRI3_CONCURRENCY_SHORT_NAMES seq par)
set(CGALPY_TRI3_CONCURRENCY_NAMES sequential parallel)
# Default
SET(CGALPY_TRI3_CONCURRENCY ${CGALPY_TRI3_CONCURRENCY_SEQUENTIAL} CACHE INTERNAL "")
SET(CGALPY_TRI3_CONCURRENCY_NAME "sequential" CACHE STRING "The 3D Triangulation concurrency tag")
set_property(CACHE CGALPY_TRI3_CONCURRENCY_NAME PROPERTY STRINGS sequential parallel)

# 3D Triangulation location policy
set(CGALPY_TRI3_LOCATION_POLICY_FAST 0)
set(CGALPY_TRI3_LOCATION_POLICY_COMPACT 1)

set(CGALPY_TRI3_LOCATION_POLICY_SHORT_NAMES fas com)
set(CGALPY_TRI3_LOCATION_POLICY_NAMES fast compact)

# Default
SET(CGALPY_TRI3_LOCATION_POLICY ${CGALPY_TRI3_LOCATION_POLICY_COMPACT} CACHE INTERNAL "")
SET(CGALPY_TRI3_LOCATION_POLICY_NAME "compact" CACHE STRING "The 3D Triangulation point location strategy")
set_property(CACHE CGALPY_TRI3_LOCATION_POLICY_NAME PROPERTY STRINGS fast compact)

function(select_tri3_concurrency)
  # Select 3D triangulation concurrency
  if     ("${CGALPY_TRI3_CONCURRENCY_NAME}" STREQUAL "sequential")
    set(CGALPY_TRI3_CONCURRENCY ${CGALPY_TRI3_CONCURRENCY_SEQUENTIAL} CACHE INTERNAL "")
  elseif ("${CGALPY_TRI3_CONCURRENCY_NAME}" STREQUAL "parallel")
    set(CGALPY_TRI3_CONCURRENCY ${CGALPY_TRI3_CONCURRENCY_PARALLEL} CACHE INTERNAL "")
  endif()
  add_definitions(-DCGALPY_TRI3_CONCURRENCY=${CGALPY_TRI3_CONCURRENCY})
endfunction()

function(select_tri3_location_policy)
  if     ("${CGALPY_TRI3_LOCATION_POLICY_NAME}" STREQUAL "fast")
    set(CGALPY_TRI3_LOCATION_POLICY ${CGALPY_TRI3_LOCATION_POLICY_FAST} CACHE INTERNAL "")
  elseif ("${CGALPY_TRI3_LOCATION_POLICY_NAME}" STREQUAL "compact")
    set(CGALPY_TRI3_LOCATION_POLICY ${CGALPY_TRI3_LOCATION_POLICY_COMPACT} CACHE INTERNAL "")
  endif()
  add_definitions(-DCGALPY_TRI3_LOCATION_POLICY=${CGALPY_TRI3_LOCATION_POLICY})
endfunction()

function(select_tri3)
  if    ("${CGALPY_TRI3_NAME}" STREQUAL "plain")
    set(CGALPY_TRI3 ${CGALPY_TRI3_PLAIN} CACHE INTERNAL "")
  elseif("${CGALPY_TRI3_NAME}" STREQUAL "regular")
    set(CGALPY_TRI3 ${CGALPY_TRI3_REGULAR} CACHE INTERNAL "")
  elseif("${CGALPY_TRI3_NAME}" STREQUAL "delaunay")
    set(CGALPY_TRI3 ${CGALPY_TRI3_DELAUNAY} CACHE INTERNAL "")
  elseif("${CGALPY_TRI3_NAME}" STREQUAL "periodicPlain")
    set(CGALPY_TRI3 ${CGALPY_TRI3_PERIODIC_PLAIN} CACHE INTERNAL "")
  elseif("${CGALPY_TRI3_NAME}" STREQUAL "periodicRegular")
    set(CGALPY_TRI3 ${CGALPY_TRI3_PERIODIC_REGULAR} CACHE INTERNAL "")
  elseif("${CGALPY_TRI3_NAME}" STREQUAL "periodicDelaunay")
    set(CGALPY_TRI3 ${CGALPY_TRI3_PERIODIC_DELAUNAY} CACHE INTERNAL "")
  endif()
  add_definitions(-DCGALPY_TRI3=${CGALPY_TRI3})
endfunction()

function(select_triangulation_3)
  if (${CGALPY_TRIANGULATION_3_BINDINGS})
    select_tri3()
    select_tri3_concurrency()
    select_tri3_location_policy()
    if (${CGALPY_TRI3_VERTEX_WITH_INFO})
      add_definitions(-DCGALPY_TRI3_VERTEX_WITH_INFO=)
    endif()
    if (${CGALPY_TRI3_FACE_WITH_INFO})
      add_definitions(-DCGALPY_TRI3_FACE_WITH_INFO=)
    endif()
    if (${CGALPY_TRI3_HIERARCHY})
      add_definitions(-DCGALPY_TRI3_HIERARCHY=)
    endif()
    add_definitions(-DCGALPY_TRIANGULATION_3_BINDINGS=)
  endif()
endfunction()

function(get_triangulation_3_lib_name ret)
  list(GET CGALPY_TRI3_SHORT_NAMES ${CGALPY_TRI3} part1)
  capitalize_first(part1)
  list(GET CGALPY_TRI3_LOCATION_POLICY_SHORT_NAMES ${CGALPY_TRI3_LOCATION_POLICY} part2)
  capitalize_first(part2)
  list(GET CGALPY_TRI3_CONCURRENCY_SHORT_NAMES ${CGALPY_TRI3_CONCURRENCY} part3)
  capitalize_first(part3)
  set(${ret} "tri3${part1}${part2}${part3}" PARENT_SCOPE)
endfunction()

endif()
