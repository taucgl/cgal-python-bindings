if(NOT KERNEL_OPTIONS_FILE_INCLUDED)
set(KERNEL_OPTIONS_FILE_INCLUDED)

# Options
set(CGALPY_KERNEL_EPIC 0)
set(CGALPY_KERNEL_EPEC 1)
set(CGALPY_KERNEL_EPEC_WITH_SQRT 2)
set(CGALPY_KERNEL_FILTERED_SIMPLE_CARTESIAN_DOUBLE 3)
set(CGALPY_KERNEL_FILTERED_SIMPLE_CARTESIAN_LAZY_GMPQ 4)
set(CGALPY_KERNEL_CARTESIAN_CORE_RATIONAL 5)
set(CGALPY_KERNEL_EXACT_CIRCULAR_KERNEL_2 6)

# Names
set(CGALPY_KERNEL_SHORT_NAMES "epic" "epec" "epecws" "fscd" "fsclg" "ccr" "ec2")
set(CGALPY_KERNEL_NAMES "epic" "epec" "epecWithSqrt" "filteredSimpleCartesianDouble" "filteredSimpleCartesianLazyGmpq" "cartesianCoreRational" "exactCircular2")

# Default
# SET(CGALPY_KERNEL_NAME "epec" CACHE STRING "The kernel to use") # TODO: ifndef def this
# set(CGALPY_KERNEL ${CGALPY_KERNEL_EPEC} CACHE INTERNAL "")
if(NOT CGALPY_KERNEL_NAME)
  set(CGALPY_KERNEL_NAME "epec" CACHE STRING "The kernel to use")
  set(CGALPY_KERNEL ${CGALPY_KERNEL_EPEC} CACHE INTERNAL "")
endif()
set_property(CACHE CGALPY_KERNEL_NAME PROPERTY STRINGS epic epec epecWithSqrt filteredSimpleCartesianDouble filteredSimpleCartesianLazyGmpq cartesianCoreRational)

# Selection
function(select_kernel_intersection)
  if(${CGALPY_KERNEL_INTERSECTION_BINDINGS})
    add_definitions(-DCGALPY_KERNEL_INTERSECTION_BINDINGS=)
  endif()
endfunction()

function(select_kernel)
  if (CGALPY_KERNEL_BINDINGS)
    if ("${CGALPY_KERNEL_NAME}" STREQUAL "epic")
      set(CGALPY_KERNEL ${CGALPY_KERNEL_EPIC} CACHE INTERNAL "")
    elseif ("${CGALPY_KERNEL_NAME}" STREQUAL "epec")
      set(CGALPY_KERNEL ${CGALPY_KERNEL_EPEC} CACHE INTERNAL "")
    elseif ("${CGALPY_KERNEL_NAME}" STREQUAL "epecWithSqrt")
      set(CGALPY_KERNEL ${CGALPY_KERNEL_EPEC_WITH_SQRT} CACHE INTERNAL "")
    elseif ("${CGALPY_KERNEL_NAME}" STREQUAL "filteredSimpleCartesianDouble")
      set(CGALPY_KERNEL ${CGALPY_KERNEL_FILTERED_SIMPLE_CARTESIAN_DOUBLE} CACHE INTERNAL "")
    elseif ("${CGALPY_KERNEL_NAME}" STREQUAL "filteredSimpleCartesianLazyGmpq")
      set(CGALPY_KERNEL ${CGALPY_KERNEL_FILTERED_SIMPLE_CARTESIAN_LAZY_GMPQ} CACHE INTERNAL "")
    elseif ("${CGALPY_KERNEL_NAME}" STREQUAL "cartesianCoreRational")
      set(CGALPY_KERNEL ${CGALPY_KERNEL_CARTESIAN_CORE_RATIONAL} CACHE INTERNAL "")
    elseif("${CGALPY_KERNEL_NAME}" STREQUAL "exactCircular2")
      set(CGALPY_KERNEL ${CGALPY_KERNEL_EXACT_CIRCULAR_KERNEL_2} CACHE INTERNAL "")
    else()
      message(FATAL_ERROR "Unknown kernel ${CGALPY_KERNEL_NAME}")
    endif()
    add_definitions(-DCGALPY_KERNEL=${CGALPY_KERNEL})
    select_kernel_intersection()
    add_definitions(-DCGALPY_KERNEL_BINDINGS=)
  endif()
endfunction()

function(get_kernel_lib_name ret)
  list(GET CGALPY_KERNEL_SHORT_NAMES ${CGALPY_KERNEL} part1)
  capitalize_first(part1)
  if(${CGALPY_KERNEL_INTERSECTION_BINDINGS})
    set(part2 "Int")
  endif()
  set(${ret} "ker${part1}${part2}" PARENT_SCOPE)
endfunction()

endif()
